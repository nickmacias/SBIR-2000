#include <stdio.h>
/*
 * Dynamic Routing Test
 */

#define ROWS 5
#define COLS 5	/* Dims for grid */

struct node {
	int failed;
	int din_n,din_s,din_w,din_e,
	    r1_n,r1_s,r1_w,r1_e,
	    lin_n,lin_s,lin_w,lin_e,
	    dout_n,dout_s,dout_w,dout_e,
	    lout_n,lout_s,lout_w,lout_e;}
	grid[ROWS][COLS];

main()
{
  int i,j;
  char buffer[120],dir;

  init();
  printf("> ");
  while (0 != gets(buffer)){
    switch (buffer[0]){
      case 'q':exit();
      case 's':step();break;
      case 'f':sscanf(buffer,"f %d %d",&i,&j);
               fail(i,j);break;
      case 'i':init();break;
      case 'o':output();break;
      case 't':sscanf(buffer,"t %d %d %c",&i,&j,&dir);
               toggle(i,j,dir);break;
      default:help();
    }
    printf("> ");
  }
}

/* Step the simulation */
step()
{
  prop();
  set_lout();
  prop();

  phi1();
  prop();
  set_lout();
  prop();

  phi2();

  output();
}

fail(i,j)
int i,j;
{
  grid[i][j].failed=1;
}

toggle(i,j,dir)
int i,j;
char dir;
{
  switch (dir){
    case 'n':grid[i][j].din_n=1-grid[i][j].din_n;break;
    case 's':grid[i][j].din_s=1-grid[i][j].din_s;break;
    case 'w':grid[i][j].din_w=1-grid[i][j].din_w;break;
    case 'e':grid[i][j].din_e=1-grid[i][j].din_e;break;
    default:printf("Unknown direction:\"%c\"\n",dir);
  }
printf("toggling dir %c, cell %d %d\n",dir,i,j);
}

help()
{
  printf("i            Initialize grid\n");
  printf("s            Step simulation and output current grid state\n");
  printf("o            Output current grid state\n");
  printf("f x y        mark cell[x,y] as failed\n");
  printf("t x y dir    Toggle input (dir) on cell[x,y]\n");
  printf("q            Exit simulator\n");
  printf("\n");
}
/* Initialize the structure */
init()
{
  int i,j;
  for (i=0;i<ROWS;i++){
    for(j=0;j<COLS;j++){
      grid[i][j].din_n=grid[i][j].din_s=grid[i][j].din_w=grid[i][j].din_e=0;
      grid[i][j].lin_n=grid[i][j].lin_s=grid[i][j].lin_w=grid[i][j].lin_e=0;
      grid[i][j].r1_n=grid[i][j].r1_s=grid[i][j].r1_w=grid[i][j].r1_e=0;
      grid[i][j].dout_n=grid[i][j].dout_s=grid[i][j].dout_w=grid[i][j].dout_e=0;
      grid[i][j].lout_n=grid[i][j].lout_s=grid[i][j].lout_w=grid[i][j].lout_e=0;
      grid[i][j].failed=0;
    }
  }
}

/* Set lout_x=OR(All DOut) and Not DIn_x */
set_lout()
{
  int i,j,dor;
  for (i=0;i<ROWS;i++){
    for (j=0;j<COLS;j++){
      dor=0;
      if ((grid[i][j].dout_n == 1) ||
          (grid[i][j].dout_s == 1) ||
          (grid[i][j].dout_w == 1) ||
          (grid[i][j].dout_e == 1)) dor=1;

      grid[i][j].lout_n=grid[i][j].lout_s=grid[i][j].lout_w=grid[i][j].lout_e=0;
      if (grid[i][j].failed == 0){
        if ((dor==1) && (grid[i][j].din_n == 0)) grid[i][j].lout_n=1;
        if ((dor==1) && (grid[i][j].din_s == 0)) grid[i][j].lout_s=1;
        if ((dor==1) && (grid[i][j].din_w == 0)) grid[i][j].lout_w=1;
        if ((dor==1) && (grid[i][j].din_e == 0)) grid[i][j].lout_e=1;
      }
    }
  }
}

/* Propogate new outputs to other cells' inputs */
prop()
{
  int i,j;
  for (i=0;i<ROWS;i++){
    for(j=0;j<COLS;j++){
      if (i > 0){ /* Copy from N out to (i-1) S in */
        grid[i-1][j].din_s = grid[i][j].dout_n;
        grid[i-1][j].lin_s = grid[i][j].lout_n;
      }
      if (i < ROWS-1){ /* Copy from S out to (i+1) N in */
        grid[i+1][j].din_n = grid[i][j].dout_s;
        grid[i+1][j].lin_n = grid[i][j].lout_s;
      }
      if (j > 0){ /* Copy from W out to (j-1) E in */
        grid[i][j-1].din_e = grid[i][j].dout_w;
        grid[i][j-1].lin_e = grid[i][j].lout_w;
      }
      if (j < COLS-1){ /* Copy from E out to (j+1) W in */
        grid[i][j+1].din_w = grid[i][j].dout_e;
        grid[i][j+1].lin_w = grid[i][j].lout_e;
      }
    }
  }
}

/* Do phi1 clock strobe */
phi1()
{
  int i,j;
  for (i=0;i<ROWS;i++){
    for (j=0;j<COLS;j++){
      grid[i][j].dout_n=grid[i][j].r1_n;
      grid[i][j].dout_s=grid[i][j].r1_s;
      grid[i][j].dout_w=grid[i][j].r1_w;
      grid[i][j].dout_e=grid[i][j].r1_e;
    }
  }
}

/* Do phi2 clock strobe */
phi2()
{
  int i,j,dor;

  for (i=0;i<ROWS;i++){
    for (j=0;j<COLS;j++){
      grid[i][j].r1_n=grid[i][j].r1_s=grid[i][j].r1_w=grid[i][j].r1_e=0;
      if (grid[i][j].failed==0){
        dor=0;
        if ((grid[i][j].din_n == 1) ||
            (grid[i][j].din_s == 1) ||
            (grid[i][j].din_w == 1) ||
            (grid[i][j].din_e == 1)) dor=1;
        if (dor==1){
          if (grid[i][j].lin_n==0) grid[i][j].r1_n=1;
          if (grid[i][j].lin_s==0) grid[i][j].r1_s=1;
          if (grid[i][j].lin_w==0) grid[i][j].r1_w=1;
          if (grid[i][j].lin_e==0) grid[i][j].r1_e=1;
        }
      }
    }
  }
}

/* Display current grid state */
output()
{
  int i,j;

  for (i=0;i<ROWS;i++){
    for (j=0;j<COLS;j++) printf("  %d  |",grid[i][j].dout_n);
    printf("\n");
    for (j=0;j<COLS;j++) printf("%d %c %d|",
                             grid[i][j].dout_w,
                             grid[i][j].failed?'*':' ',
                             grid[i][j].dout_e);
    printf("\n");
    for (j=0;j<COLS;j++) printf("  %d  |",grid[i][j].dout_s);
    printf("\n");
    for (j=0;j<COLS;j++) printf("-----+");
    printf("\n");
  }
}
